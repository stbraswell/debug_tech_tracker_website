// Laod Page to main page
function loadPageToMain(file){
  document.getElementById("main").style.visibility="visible";
  $('#main').load(file);
  document.getElementById('Text1').focus();
}
var clicks = 0;
var SERVER = window.location.href.split('/')[2];

PORT=':5000';
var getMain_API = "http://" + SERVER + PORT + "/api/v1/resources/filter?get=main";
var getQuarterly_API = "http://" + SERVER + PORT + "/api/v1/resources/filter?get=quarterly";
var getWeekly_API = "http://" + SERVER + PORT + "/api/v1/resources/filter?get=weekly";
var getMonthly_API = "http://" + SERVER + PORT + "/api/v1/resources/filter?get=monthly";

// Easter Egg ;)
function onClick() {
	clicks += 1;

	if (clicks == 5) {
		var vischeck = document.getElementById("inputslider").style.visibility
		if (vischeck == "hidden"){
			document.getElementById("inputslider").style.visibility="visible";
			document.getElementById("inputslider").innerHTML = '<a href="admin.html" id="admin">ADMIN</a>'; 
			$("#inputslider").slideDown("slow");
					
		}else{
			document.getElementById("inputslider").style.visibility="hidden";
			document.getElementById("inputslider").innerHTML = "";
			$("#inputslider").slideUp("slow");
		}
		clicks = 0
	}
};

// Provides html for viewing all entries in DB
function getMain() {
	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {
		// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById("accuracyContainer").innerHTML = this.responseText;
		}
	};
	xmlhttp.open("GET",getMain_API,true);
	xmlhttp.send();
}

function getIndivQ(quarter) {
	
	var getIndivQ_API = "http://" + SERVER + PORT + "/api/v1/resources/filter?get=" + quarter;
	var colorList = ["rgba(131, 170, 212,1)","rgba(53, 199, 194,1)","rgba(255, 255, 0,1)","rgba(255, 0, 255,1)","rgba(36, 113, 163,1)","rgba(255, 193, 7,1)","rgba(51, 204, 51,1)","rgba(157, 19, 212,1)","rgba(53, 199, 194,1)"]
	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {
		// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var myObj = JSON.parse(this.responseText);
			document.getElementById("theCards").innerHTML = myObj.table;
			// Get datasets for top 5 Fails with breakdown
			var stackedDatasets = [];
			var i = 0;
			for (var key in myObj.failsWithTest) {
				stackedDatasets[i]={
					label: key,
					backgroundColor: colorList[i],
					data: myObj.failsWithTest[key][0],
					stackedFixes: myObj.failsWithTest[key][1]
				} 
				i++;
			}
			
			
			
			// Define a plugin to provide data labels for Accuracy Per Tech Chart
			//Chart.plugins.register({
			var plugin ={
				afterDatasetsDraw: function(chart, easing) {
					// To only draw at the end of animation, check for easing === 1
					var ctx = chart.ctx;
					chart.data.datasets.forEach(function (dataset, i) {
						var meta = chart.getDatasetMeta(i);
						if (!meta.hidden) {
							meta.data.forEach(function(element, index) {
								// Draw the text in black, with the specified font
								ctx.fillStyle = 'rgb(0, 0, 0)';
								var fontSize = 16;
								var fontStyle = 'normal';
								var fontFamily = 'Helvetica Neue';
								ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
								// Just naively convert to string for now
								
								var dataString = dataset.numbers[index].toString();
								if (dataString === '0') {
									dataString = ' ';
								}
								// Make sure alignment settings are correct
								ctx.textAlign = 'center';
								ctx.textBaseline = 'middle';
								var padding = 5;
								var position = element.tooltipPosition();
								ctx.fillText(dataString, position.x, position.y- (fontSize / 2) + (padding*5));
								
							});
						}
					});
				}
			};
			
			//ACCURACY PER TECH
			var barChartData = {
				labels: myObj.Labels,
				datasets: [{
					label: '% Pass',
					backgroundColor: "rgba(51, 204, 51,1)",
					data: myObj.Accs,
					numbers: myObj.numPass
				}, {
					label: '% Fail',
					backgroundColor: window.chartColors.red,
					data: myObj.Fails,
					numbers: myObj.numFail
				}]
			};
		
			var ctx = document.getElementById('canvas').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				plugins: [plugin],
				data: barChartData,
				options: {
					title: {
						display: true,
						text: 'Accuracy Per Tech'
					},
					tooltips: {
						mode: 'index',
						intersect: false
					},
					responsive: true,
					scales: {
						xAxes: [{
							stacked: true,
						}],
						yAxes: [{
							stacked: true
						}]
					}
				}
			});
			
			
			//Productivity PER TECH
			var barChartData_prod = {
				labels: myObj.prod_labels,
				datasets: [{
					label: '# of Touches',
					backgroundColor: "rgba(51, 204, 51,1)",
					data: myObj.prod_nums,
				}]
			};
		
			var ctx = document.getElementById('canvas_prod').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barChartData_prod,
				options: {
					title: {
						display: true,
						text: 'Touches Per Tech'
					},
					tooltips: {
						mode: 'index',
						intersect: false
					},
					responsive: true,
				}
			});

			//TOP 5 FIXES
			var barChartDataFixes = {
				labels: myObj.topFixes.labels,
				datasets: [{
					label: 'Fixes',
					backgroundColor: "rgba(51, 204, 51,1)",
					data: myObj.topFixes.counts,
				} ]
			};
			var ctx2 = document.getElementById('canvas_fixes').getContext('2d');
			window.myBar = new Chart(ctx2, {
				type: 'bar',
				data: barChartDataFixes,
				options: {
					title: {
						display: true,
						text: 'Top 5 Fixes',
						fontStyle: 'bold',
						fontSize: 25
					},
					tooltips: {
						mode: 'index',
						intersect: false
					},
					responsive: true,
					scales: {
						xAxes: [{
							stacked: true,
						}],
						yAxes: [{
							stacked: true
						}]
					}
				}
			}); 
		
			

			//TOP 5 FAILS WITH TEST BREAKDOWN
			var barChartDataFailswithTest = {
				labels: myObj.topFails.labels,
				datasets: stackedDatasets
			};
			var ctx4 = document.getElementById('canvas_failstests').getContext('2d');
			window.myBar = new Chart(ctx4, {
				type: 'bar',
				data: barChartDataFailswithTest,
				options: {
					title: {
						display: true,
						text: 'Top 5 Fails with Test Breakdown',
						fontStyle: 'bold',
						fontSize: 25
					},
					tooltips: {
						mode: 'index',
						intersect: false,
						callbacks: {
							title: function(tooltipItem, data) {			// Display full failure text
								 return data.labels[tooltipItem[0]['index']];
							},
							label: function(tooltipItem, data) {			// Display only tests and count included in the failure
								if (data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] > 0){
									var label = [data.datasets[tooltipItem.datasetIndex].label + ": "];// || '';
									if (label) {
										var numFails = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] 
										label += numFails;
									}
								}
								return label;
							}, 
							footer: function(tooltipItem, data) {			// Display the breakdown of fixes per test in the footer
								var fixes = ['Fixes: '];
								var daFix = '';
								var test = '';
								for (var key in data.datasets) {
									if (data.datasets[key].stackedFixes[tooltipItem[0].index] != "") {
										test = data.datasets[key].label;
										daFix = data.datasets[key].stackedFixes[tooltipItem[0].index];
										var tmp = test + ": " + daFix;
										fixes.push(tmp);
									}
								}
							  return fixes;
							}
						} 
					},
					responsive: true,
					scales: {
						xAxes: [{
							ticks: {
								autoSkip: false,
								callback: function(label, index) {
									if (label.length > 20){
										var textLabel = label.substr(0,20);
										return textLabel;
									} else {
										return label;
									}
								}
							},
							stacked: true,
						}],
						yAxes: [{
							stacked: true
						}]
					}
				}
			});
		}
	};

	xmlhttp.open("GET",getIndivQ_API,true);
	xmlhttp.send();
}

function getQuarterly() {
	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {
		// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var myObj = JSON.parse(this.responseText);
			document.getElementById("theCards").innerHTML = myObj.html;
			var color = Chart.helpers.color;
			var barChartData = {
				labels: myObj.Labels, 
				datasets: [{
					label: 'Accuracy',
					backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
					borderColor: window.chartColors.blue,
					borderWidth: 1,
					data: myObj.Accs
				}]
			};
			var ctx = document.getElementById('canvas').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
					responsive: true,
					legend: {
						position: 'top',
					},
					tooltips: {
						mode: 'index',
						intersect: false,
						callbacks: {
							label: function(tooltipItems, data) {
								return data.datasets[tooltipItems.datasetIndex].label +': ' + tooltipItems.yLabel + '%';
							}	
						} 
					},
					title: {
						display: true,
						text: 'Accuracy per Quarter'
					},
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero: true
							}
						}]
					}
				}
			});
		}
	};
	xmlhttp.open("GET",getQuarterly_API,true);
	xmlhttp.send();
}

function getWeekly() {
	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {
		// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var myObj = JSON.parse(this.responseText);
			document.getElementById("theCards").innerHTML = myObj.html;
			var color = Chart.helpers.color;
			var barChartData = {
				labels: myObj.Labels, 
				datasets: [{
					label: 'Accuracy',
					backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
					borderColor: window.chartColors.blue,
					borderWidth: 1,
					data: myObj.Accs
				}]
			};
			var ctx = document.getElementById('canvas').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
					responsive: true,
					legend: {
						position: 'top',
					},
					title: {
						display: true,
						text: 'Accuracy per Week'
					},
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero: true
							}
						}]
					}
				}
			});
		}
	};
	xmlhttp.open("GET",getWeekly_API,true);
	xmlhttp.send();
}

function getMonthly() {
	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {
		// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var myObj = JSON.parse(this.responseText);
			document.getElementById("theCards").innerHTML = myObj.html;
			var color = Chart.helpers.color;
			var barChartData = {
				labels: myObj.Labels, 
				datasets: [{
					label: 'Accuracy',
					backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
					borderColor: window.chartColors.blue,
					borderWidth: 1,
					data: myObj.Accs
				}]
			};
			var ctx = document.getElementById('canvas').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
					responsive: true,
					legend: {
						position: 'top',
					},
					title: {
						display: true,
						text: 'Accuracy per Month'
					},
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero: true
							}
						}]
					}
				}
			});
		}
	};
	xmlhttp.open("GET",getMonthly_API,true);
	xmlhttp.send();
}