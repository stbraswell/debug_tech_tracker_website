This is the front end site that displays the data of the 'debug_tech_tracker_scripts'.  It was developed so that Managers can easily see how each of their debug technicians 
is doing.  It also shows failure trends as well as any trends in components used to fix the failures.

<h1>Quarterly View</h1>
![alt text](readme_files/quarterly.png "Quarterly View")

<h1>View of Specific Quarter</h1>
![alt text](readme_files/indivdual_quarter.png "Quarter View")